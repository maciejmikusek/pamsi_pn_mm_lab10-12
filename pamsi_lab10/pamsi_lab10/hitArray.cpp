#include "stdafx.h"
#include "hitArray.h"

hitArray::hitArray()
{
	arr = new hitField*[GAMEsize];
	for (unsigned int i = 0; i < GAMEsize; ++i) {
		arr[i] = new hitField[GAMEsize];
	}
}


hitArray::~hitArray()
{
}

std::ostream& operator<< (std::ostream& os, const hitArray& obj) {
	os << "Twoje trafienia:\n";
	os << "  " << " " << " [A] [B] [C] [D] [E] [F]\n";
	for (unsigned int i = 0; i < GAMEsize; ++i) {
		os << "[" << i << "] ";
		for (unsigned int j = 0; j < GAMEsize; ++j) {
			os << "[";
			//content of ship field
			if (obj.arr[j][i].fog) { os << " "; }
			if (obj.arr[j][i].hit) { os << "V"; }
			if (obj.arr[j][i].miss) { os << "X"; }
			os << "] ";
		}
		os << std::endl;
	}
	return os;
}