#include "stdafx.h"
#include "battleInput.h"


battleInput::battleInput() 
: incolumn ('0'), inrow(0), column('0'), row(0) {}


battleInput::~battleInput() {
}

void battleInput::setOuts() {
	row = inrow;
	switch (incolumn) {
	case 'A': column = 0; break;
	case 'a': column = 0; break;
	case 'B': column = 1; break;
	case 'b': column = 1; break;
	case 'C': column = 2; break;
	case 'c': column = 2; break;
	case 'D': column = 3; break;
	case 'd': column = 3; break;
	case 'E': column = 4; break;
	case 'F': column = 5; break;
	case 'e': column = 4; break;
	case 'f': column = 5; break;
	default:  column = 404;
	}
}


std::istream& operator>> (std::istream& is, battleInput& bi) {
	is >> bi.incolumn >> bi.inrow;
	bi.setOuts();
	if (bi.column == 404  || bi.row < 0 || bi.row > GAMEsize - 1)
		is.setstate(std::ios::failbit);
	return is;
}