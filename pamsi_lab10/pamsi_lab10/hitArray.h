#pragma once
#include <iostream>
#include "global.h"



class hitArray {
	friend std::ostream& operator<< (std::ostream&, const hitArray&);
	friend class player;

	struct hitField {

		hitField() : fog(true), hit(false), miss(false) {}

		void setFog() { fog = true; hit = false; miss = false; }
		void setHit() { fog = false; hit = true; miss = false; }
		void setMiss() { fog = false; hit = false; miss = true;}

		bool fog;		// unknown, this is the fog of war
		bool hit;		// there used to be a ship here, but it got doinked
		bool miss;		// known empty space, the shot missed!
	};
public:
	hitArray();
	~hitArray();
	hitField** garr() const { return arr; }
private:
	hitField** arr;
};

