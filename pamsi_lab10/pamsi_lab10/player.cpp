#include "stdafx.h"
#include "player.h"

player::player(std::string nname) : name(nname){
	system("CLS");
	std::cout << "Witaj w battleship_pamsi 1.0. Jestes " << name << ".\nSzybka legenda:\n";
	std::cout << "[] - puste pole\nV - trafiony statek przeciwnika\nX - pudlo\n+ - czesc statku w dobrym stanie\n";
	std::cout << "/ - zniszczona czesc statku\n";
	std::cout << "Zacznijmy od rozstawienia floty.\n";
	system("pause");
	for (unsigned int i = numOfShips + 1; i > 1; --i) {
		allies.placeShip(i);
	}
	system("cls");
	std::cout << allies;
	std::cout << "Teraz ustap drugiemu graczowi.\n";
	system("pause");
}


player::~player() {
}

bool player::isDead() const{
	if (sumOfArray(allies.ships, numOfShips+2) == 0) return true;
	return false;
}

const std::string& player::gname() const {
	return name;
}

void game() {
	player P1("Player 1");
	player P2("Player 2");
	battleInput target1;
	battleInput target2;



	while (!P1.isDead() && !P2.isDead()) {
		passTurn();
		//P1 turn
		system("cls");
		std::cout << "Tura " << P1.gname() << "..." << std::endl;
		std::cout << P1.allies;
		std::cout << P1.hostiles;
		std::cout << "Oddaj strzal korzystajac z planszy trafien, podaj cel:\n";
		target1 = userInput();

		if (P2.allies.garr()[target1.column][target1.row].noShip) { // miss!
			P1.hostiles.garr()[target1.column][target1.row].setMiss();
			std::cout << "Pudlo!\n";
		}
		else if (P2.allies.garr()[target1.column][target1.row].destroyedShip) { // wasted ur turn
			std::cout << "Juz unicestwiles ta czesc statku. Pudlo!\n";
		}
		else if (P2.allies.garr()[target1.column][target1.row].ship) {		// hit!
			P1.hostiles.garr()[target1.column][target1.row].setHit(); // set field to hit
			P2.allies.garr()[target1.column][target1.row].setDestroyedShip();
			if (!P2.allies.isSunk(P2.allies.garr()[target1.column][target1.row].whatShip())) { // if not sunk before
				--P2.allies.gships()[P2.allies.garr()[target1.column][target1.row].whatShip()];	// decrease ship health
				if (P2.allies.isSunk(P2.allies.garr()[target1.column][target1.row].whatShip())) { // if sunk now
					std::cout << "Trafiony i zatopiony!! Zatopiles statek o dlugosci "
						<< P2.allies.garr()[target1.column][target1.row].whatShip() << ".\n"; // what ship?
				}
				else {
					std::cout << "Trafiony!\n";
				}
			}
		}
		std::cout << P1.hostiles;
		system("pause");

		//P2 turn
		if (P2.isDead()) break;

		passTurn();
		system("cls");
		std::cout << "Tura " << P2.gname() << "..."<< std::endl;
		std::cout << P2.allies;
		std::cout << P2.hostiles;
		std::cout << "Oddaj strzal korzystajac z planszy trafien, podaj cel:\n";
		target2 = userInput();

		if (P1.allies.garr()[target2.column][target2.row].noShip) { // miss!
			P2.hostiles.garr()[target2.column][target2.row].setMiss();
			std::cout << "Pudlo!\n";
		}
		else if (P1.allies.garr()[target2.column][target2.row].destroyedShip) { // wasted ur turn
			std::cout << "Juz unicestwiles ta czesc statku. Pudlo!\n";
		}
		else if (P1.allies.garr()[target2.column][target2.row].ship) {		// hit!
			P2.hostiles.garr()[target2.column][target2.row].setHit(); // set field to hit
			P1.allies.garr()[target2.column][target2.row].setDestroyedShip();
			if (!P1.allies.isSunk(P1.allies.garr()[target2.column][target2.row].whatShip())) { // if not sunk before
				--P1.allies.gships()[P1.allies.garr()[target2.column][target2.row].whatShip()];	// decrease ship health
				if (P1.allies.isSunk(P1.allies.garr()[target2.column][target2.row].whatShip())) { // if sunk now
					std::cout << "Trafiony i zatopiony!! Zatopiles statek o dlugosci "
						<< P1.allies.garr()[target2.column][target2.row].whatShip() << ".\n"; // what ship?
				}
				else {
					std::cout << "Trafiony!\n";
				}
			}
		}
		std::cout << P2.hostiles;
		system("pause");
	}
	system("cls");
	if (P2.isDead()) std::cout << "KONIEC GRY!\nWygrywa " << P1.gname() << ".\n";
	else if (P1.isDead()) std::cout << "KONIEC GRY!\nWygrywa " << P2.gname() << ".\n";
}