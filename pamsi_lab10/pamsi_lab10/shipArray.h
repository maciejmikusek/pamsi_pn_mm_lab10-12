#pragma once
#include <iostream>
#include "battleInput.h"
#include "other.h"
#include "global.h"

class shipArray {
	friend std::ostream& operator<< (std::ostream&, const shipArray&);
	friend class player;

	struct shipField {

		shipField() : noShip(true), ship(false), destroyedShip(false), shipSize(0) {}

		void setNoShip() { noShip = true; ship = false; destroyedShip = false; }
		void setShip() { noShip = false; ship = true; destroyedShip = false; }
		void setDestroyedShip() { noShip = false; ship = false; destroyedShip = true; }
		 int whatShip() { return shipSize; }

		bool noShip;		// there is nothing here, its a miss!
		bool ship;			// there is a ship here
		bool destroyedShip;	// there used to be a ship here, but it got doinked
		 int shipSize;
	};

public:
	shipArray();
	~shipArray();
	void placeShip(const  int& sizeOfShip);
	bool isSunk(const int& which) const;
	shipField** garr() const { return arr; }
	int* gships() const { return ships; }
private:
	shipField** arr;
	 int* ships;
};

