#include "stdafx.h"
#include "shipArray.h"



shipArray::shipArray() {
	arr = new shipField*[GAMEsize];
	for (int i = 0; i < GAMEsize; ++i) {
		arr[i] = new shipField[GAMEsize];
	}
	ships = new int[numOfShips+2];
	for (int i = 0; i < numOfShips+2; ++i) {
		ships[i] = 0;
	}
}


shipArray::~shipArray() {


}

std::ostream& operator<< (std::ostream& os, const shipArray& obj) {
	os << "Stan floty:\n";
	os << "  " << " " << " [A] [B] [C] [D] [E] [F]\n";
	for (int i = 0; i < GAMEsize; ++i) {
		os<<"[" << i << "] ";
		for (int j = 0; j < GAMEsize; ++j) {
			os << "[";
			//content of ship field
			if (obj.arr[j][i].noShip) { os << " "; }
			if (obj.arr[j][i].ship) { os << "+"; }
			if (obj.arr[j][i].destroyedShip) { os << "/"; }



			os << "] ";
		}
		os << std::endl;
	}
	return os;
}

void shipArray::placeShip(const int& sizeOfShip) {
	bool collision = false;
	bool samePos = false;
	bool diagonal = false;
	bool tooFar = false;
	int i;
	int j; // helpers for loops
	ships[sizeOfShip] = sizeOfShip; // start_HP for each ship is it's size
	battleInput pos1;
	battleInput pos2;

	system("cls");
	std::cout << *this;
	do {
		collision = false;
		samePos = false;
		diagonal = false;
		tooFar = false;

		std::cout << "Poloz na planszy statek o dlugosci " << sizeOfShip <<
			".\nPodaj pole, na ktorym spocznie pierwszy koniec:\n";
		pos1 = userInput();
		std::cout << "Teraz drugi koniec:\n";
		pos2 = userInput();
		// checking for collisions below:
		// vertical: //iterates on i (right index)
		if (pos1.column == pos2.column && pos1.row == pos2.row) samePos = true;
		if (pos1.column != pos2.column && pos1.row != pos2.row) diagonal = true;
		if (pos1.column == pos2.column) {
			if (pos1.row < pos2.row) {
				if (pos2.row - pos1.row != sizeOfShip - 1) tooFar = true;
				i = pos1.row;
				for (int p = 0; p < sizeOfShip; ++p) {
					if (tooFar) break;
					if (arr[pos1.column][i].ship) collision = true;
					++i;
				}
			}
			else if (pos1.row > pos2.row) {
				if (pos1.row - pos2.row != sizeOfShip - 1) tooFar = true;
				i = pos2.row;
				for (int p = 0; p < sizeOfShip; ++p) {
					if (tooFar) break;
					if (arr[pos1.column][i].ship) collision = true;
					++i;
				}
			}
		}
		else if (pos1.row == pos2.row) { // horizontal // iterates on j (left index, letter)
			if (pos1.column < pos2.column) {
				if (pos2.column - pos1.column != sizeOfShip - 1) tooFar = true;
				j = pos1.column;
				for (int p = 0; p < sizeOfShip; ++p) {
					if (tooFar) break;
					if (arr[j][pos1.row].ship) collision = true;
					++j;
				}
			}
			else if (pos1.column > pos2.column) {
				if (pos1.column - pos2.column != sizeOfShip - 1) tooFar = true;
				j = pos2.column;
				for (int p = 0; p < sizeOfShip; ++p) {
					if (tooFar) break;
					if (arr[j][pos1.row].ship) collision = true;
					++j;
				}
			}
		}
		// end of checking for samePos and collisions 
		if (collision) std::cout << "Wykryto kolizje! Upewnij sie, ze kladziesz statek w wolnym miejscu!\n";
		if (samePos) std::cout << "Podales ta sama pozycje dwa razy!\n";
		if (diagonal) std::cout << "Nie mozna polozyc statku skosnie!\n";
		if (tooFar) std::cout << "Podales zbyt odlegle lub zbyt bliskie pozycje!\n";
		if (collision || samePos || diagonal || tooFar) std::cout << "Sprobuj ponownie.\n";
	} while (collision || tooFar || diagonal || samePos);




	// actual placing below
	if (!collision && !samePos) {	// vertical: //iterates on i (right index)
		if (pos1.column == pos2.column) {
			if (pos1.row < pos2.row) {
				i = pos1.row;
				for (int p = 0; p < sizeOfShip; ++p) {
					arr[pos1.column][i].setShip();
					arr[pos1.column][i].shipSize = sizeOfShip;
					++i;
				}
			}
			else if (pos1.row > pos2.row) {
				i = pos2.row;
				for (int p = 0; p < sizeOfShip; ++p) {
					arr[pos1.column][i].setShip();
					arr[pos1.column][i].shipSize = sizeOfShip;
					++i;
				}
			}
		}
		else if (pos1.row == pos2.row) { // horizontal // iterates on j (left index, letter)
			if (pos1.column < pos2.column) {
				j = pos1.column;
				for (int p = 0; p < sizeOfShip; ++p) {
					arr[j][pos1.row].setShip();
					arr[j][pos1.row].shipSize = sizeOfShip;
					++j;
				}
			}
			else if (pos1.column > pos2.column) {
				j = pos2.column;
				for (int p = 0
					; p < sizeOfShip; ++p) {
					arr[j][pos1.row].setShip();
					arr[j][pos1.row].shipSize = sizeOfShip;
					++j;
				}
			}
		}
	}

}

bool shipArray::isSunk(const int& which) const {
	if (ships[which] == 0) return true;
	return false;
}