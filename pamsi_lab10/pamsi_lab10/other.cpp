#include "stdafx.h"
#include "other.h"
#include "global.h"

battleInput userInput() {
	battleInput bi;
	std::cin >> bi;
	while (std::cin.fail()) {
		std::cin.clear();
		std::cin.ignore();
		std::cout << "Nieprawidlowy input, skrajne pola to A0, A5, F0 i F5.\n";
		std::cin >> bi;
	}
	return bi;
}

int sumOfArray(int* pt, const int& arrSize) {
	int sum = 0;
	for (int k = 0; k < arrSize; ++k) {
		sum += pt[k];
	}
	return sum;
}

void passTurn() {
	system("cls");
	std::cout << "Przekazanie tury.\n";
	system("pause");
}