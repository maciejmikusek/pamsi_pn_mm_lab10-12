#pragma once
#include "battleInput.h"
#include "global.h"
#include "shipArray.h"
#include "hitArray.h"
#include <string>

class player {

	friend void game();

public:
	player(std::string);
	~player();
	bool isDead() const;
	const std::string& gname() const;
private:
	std::string name;

	hitArray hostiles;
	shipArray allies;
};

