
#pragma once
#include "stdafx.h"
#include <string>


class customEx {
	std::string msg;
public:
	customEx()
		: msg("This is a custom exception!") {}
	customEx(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};