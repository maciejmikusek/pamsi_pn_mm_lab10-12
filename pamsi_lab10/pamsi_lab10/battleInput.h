#pragma once
#include <iostream>
#include "global.h"



class battleInput {

	friend std::istream& operator>> (std::istream&, battleInput&);

public:
	battleInput();
	~battleInput();
	void setOuts();


private:
	char incolumn;
	int inrow;

public:
	int column;
	int row;
};

